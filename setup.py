# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

INSTALL_REQUIREMENTS = [
    "requests>=2.6.0",
    "click>=7.0,<8.0",
    "pygtail>=0.11.1,<0.12",
    "jsons>=0.8,<2.0",
]

setup(
    author="Jonas Ohrstrom",
    author_email="ohrstrom@gmail.com",
    url="https://gitlab.com/ohrstrom/icewatch",
    name="icewatch",
    version="0.0.1",
    description="watches icecast2 formatted logfiles and sends events to GUA & internal API",
    packages=find_packages(),
    install_requires=INSTALL_REQUIREMENTS,
    entry_points="""
        [console_scripts]
        icewatch-cli=icewatch:cli.icewatch_cli
    """,
)
