# -*- coding: utf-8 -*-
import hashlib
import requests
import jsons
from requests.exceptions import ConnectionError, ReadTimeout, RequestException
from . import __version__
from .logger import logger


CLIENT_HEADERS = {"User-Agent": "Icewatch {}".format(__version__)}


def send_event_to_ga(event, analytics_id):

    cid = hashlib.md5(
        "{}:{}".format(event.ip, event.user_agent).encode("utf-8")
    ).hexdigest()

    payload = {
        "v": "1",
        "tid": analytics_id,
        "cid": cid,
        "uip": event.ip,
        "ua": event.user_agent,
        "t": "event",
        "ec": "stream",
        "ea": "listened",
        "el": event.path,
        "ev": int(event.seconds_connected),
    }

    try:
        r = requests.post(
            "https://www.google-analytics.com/collect",
            data=payload,
            headers=CLIENT_HEADERS,
            timeout=(2, 10),
        )
    except (ConnectionError, ReadTimeout, RequestException) as e:
        logger.warning("error sending event to GA: {}".format(e))
        return

    if r.status_code == 200:
        logger.debug("sent event to GA: {}".format(event))
    else:
        logger.warning("bad status code from GA: {}".format(r.status_code))


def send_event_to_webhook(event, webhook_url, webhook_token):
    logger.debug("send event to webhook: {}".format(webhook_url))

    headers = CLIENT_HEADERS.copy()
    if webhook_token:
        headers.update({"Authorization": "Token {}".format(webhook_token)})

    payload = jsons.dump(event, indent=4)

    logger.debug("headers: {}".format(headers))
    logger.debug("payload: {}".format(payload))

    try:
        r = requests.post(webhook_url, json=payload, headers=headers, timeout=(2, 10))
    except (ConnectionError, ReadTimeout, RequestException) as e:
        logger.warning("error sending event to webhook: {}".format(e))
        return

    if r.status_code in [200, 201]:
        logger.debug("sent event to webhook: {}".format(event))
    else:
        logger.warning("bad status code from webhook: {} {}".format(r.status_code, r.text))
