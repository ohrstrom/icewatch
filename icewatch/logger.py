# -*- coding: utf-8 -*-
import logging
from logging.config import fileConfig

fileConfig("logger.ini")
logger = logging.getLogger()
