# -*- coding: utf-8 -*-
from __future__ import absolute_import

import sys
import click

from icewatch import __version__

from .logger import logger
from .watcher import watch_log
from .exceptions import LogWatcherException


@click.group()
@click.option("-v", "--verbose", default=False, is_flag=True)
def icewatch_cli(verbose):
    if verbose:
        import logging

        logger.setLevel(logging.DEBUG)


@icewatch_cli.command(name="version")
def cli_version():
    click.echo(__version__)
    sys.exit()


@icewatch_cli.command(name="watch-access-log")
@click.option(
    "--log",
    "-l",
    "log_path",
    envvar="LOG_PATH",
    type=click.Path(exists=True, dir_okay=False),
    required=True,
    help="Path to icecast access log.",
)
@click.option(
    "--ignore-ip",
    "-i",
    "ignored_ip_addresses",
    type=str,
    required=False,
    multiple=True,
    help="Ignore IP address. Multiple allowed",
)
@click.option(
    "--min-seconds-connected",
    "min_seconds_connected",
    type=int,
    default=5,
    show_default=True,
    help="Skip events with shorter connection time.",
)
@click.option(
    "--sleep",
    "-s",
    "interval",
    type=int,
    default=5,
    show_default=True,
    help="Sleep for seconds after parsing.",
)
@click.option(
    "--ua",
    "analytics_id",
    envvar="ANALYTICS_ID",
    type=str,
    required=False,
    help="ANALYTICS_ID Analytics id [UA-00000000-X]",
)
@click.option(
    "--webhook-url",
    "webhook_url",
    envvar="WEBHOOK_URL",
    type=str,
    required=False,
    help="WEBHOOK_URL Webhook URL to POST event payload to.",
)
@click.option(
    "--webhook-token",
    "webhook_token",
    envvar="WEBHOOK_TOKEN",
    type=str,
    required=False,
    help="WEBHOOK_TOKEN authentication token to send with webhook request..",
)
def cli_watch_access_log(**kwargs):
    """
    Watch access log & send events to GUA (google universal analytics)

    \b
    icewatch-cli watch-access-log \\
        --log /var/log/icecast2/access.log \\
        --ua UA-00000000-X \\
        --webhook-url http://127.0.0.1:8000/event-hook/

    """

    try:
        watch_log(**kwargs)
    except LogWatcherException as e:
        click.secho("log watcher error: {}".format(e), fg="red", bold=True)
        sys.exit(1)
