# -*- coding: utf-8 -*-
import logging
import time
import re
import os
import hashlib
from datetime import datetime, timedelta
from dataclasses import dataclass
from pygtail import Pygtail

from .exceptions import LogWatcherException
from .send_events import send_event_to_ga, send_event_to_webhook

logger = logging.getLogger(__name__)


@dataclass
class Event:
    ip: str
    time_local: str
    method: str
    path: str
    status: int
    bytes_sent: int
    referer: str
    user_agent: str
    seconds_connected: float
    time_start: datetime = None
    time_end: datetime = None

    def __post_init__(self):

        if self.status:
            self.status = int(self.status)

        if self.bytes_sent:
            self.bytes_sent = int(self.bytes_sent)

        if self.seconds_connected:
            self.seconds_connected = float(self.seconds_connected)

        if self.referer == "-":
            self.referer = None

        if self.user_agent == "-":
            self.user_agent = None

        if self.time_local:
            self.time_end = datetime.strptime(self.time_local, "%d/%b/%Y:%H:%M:%S %z")
            self.time_start = self.time_end - timedelta(seconds=self.seconds_connected)

    def __repr__(self):
        return "Entry - {} {:%Y-%m-%d %H:%M:%S} - {:%Y-%m-%d %H:%M:%S} - {}".format(
            self.ip, self.time_start, self.time_end, self.user_agent
        )


class LogWatcher(object):

    line_format = re.compile(
        r"""(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) - - \[(?P<time_local>\d{2}\/\w{3}\/\d{4}:\d{2}:\d{2}:\d{2} (\+|\-)\d{4})\] \"(?P<method>\S{3,10}) (?P<path>.+) HTTP\/1\.\d" (?P<status>\d{3}) (?P<bytes_sent>\d+) (["](?P<referer>(\-)|(.+))["]) (["](?P<user_agent>.+)["]) (?P<seconds_connected>\d+(?:\.\d+)?)""",
        re.IGNORECASE,
    )

    def __init__(
        self, log_path, ignored_ip_addresses, min_seconds_connected, analytics_id, webhook_url, webhook_token
    ):
        self.log_path = log_path
        self.ignored_ip_addresses = list(ignored_ip_addresses) or []
        self.min_seconds_connected = min_seconds_connected
        self.analytics_id = analytics_id
        self.webhook_url = webhook_url
        self.webhook_token = webhook_token

        if not os.path.exists(self.log_path):
            raise LogWatcherException("path does not exist: {}".format(self.log_path))

        if self.analytics_id and not len(self.analytics_id) == 13:
            raise LogWatcherException(
                "UA id seems to be incorrect: {}".format(self.analytics_id)
            )

    def _is_relevant_event(self, entry):

        if entry.ip in self.ignored_ip_addresses:
            logger.debug('skipping entry - ip ignored: {}'.format(entry.ip))
            return False

        if entry.method in ['SOURCE']:
            logger.debug('skipping entry - method ignored: {}'.format(entry.method))
            return False

        if entry.path.startswith("/admin/"):
            return False

        if entry.path.endswith(".xsl"):
            return False

        if entry.seconds_connected < self.min_seconds_connected:
            logger.debug('skipping entry - s connected: {}'.format(entry.seconds_connected))
            return False

        return True

    def _parse_line(self, line):
        try:
            line = line.decode("utf-8")
        except (UnicodeDecodeError, AttributeError):
            pass
        parsed = re.search(self.line_format, line)
        if parsed:
            event = Event(**parsed.groupdict())
            if self._is_relevant_event(event):
                return event
        return

    def watch(self):
        events = []
        # read batch of (new) lines
        for line in Pygtail(self.log_path):
            event = self._parse_line(line)
            if event:
                events.append(event)

        # process/distribute events
        for event in events:
            if self.analytics_id:
                send_event_to_ga(event, self.analytics_id)
            if self.webhook_url:
                send_event_to_webhook(event, self.webhook_url, self.webhook_token)


def watch_log(
    log_path,
    ignored_ip_addresses=[],
    interval=5,
    min_seconds_connected=5,
    analytics_id=None,
    webhook_url=None,
    webhook_token=None,
):

    logger.info(
        "starting log watcher: path: {} - ignore: {}".format(
            log_path, ignored_ip_addresses
        )
    )

    watcher = LogWatcher(
        log_path=log_path,
        ignored_ip_addresses=ignored_ip_addresses,
        min_seconds_connected=min_seconds_connected,
        analytics_id=analytics_id,
        webhook_url=webhook_url,
        webhook_token=webhook_token,
    )

    while True:
        watcher.watch()
        time.sleep(interval)
