# Icecast2 'real time' Log Analyser & Event Creation

Watches icecast2 formatted logs and extracts 'listener' events.  
Events are sent to GUA and/or custom API endpoint.


## install

    git clone https://gitlab.com/ohrstrom/icewatch.git
    cd icewatch
    pip install -e .
    
## Usage

    icewatch-cli watch-access-log --help

## Example

    icewatch-cli watch-access-log \
    --log ./dev/test.log \
    --ignore 10.10.8.101 \
    --webhook-url https://www.openbroadcast.ch/api/v2/listener/event/ \
    --webhook-token ***
